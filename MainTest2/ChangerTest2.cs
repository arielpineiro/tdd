using MainApp;
using Xunit;

namespace MainTest2
{
    public class ChangerTest2
    {
        /// <summary>
        /// Whens the input is zero output must be 0.
        /// </summary>
        [Fact]
        public void When_Input_Is_Zero_Output_Must_Be_0()
        {
            int input = 0;

            string output = Changer.GetValue(input);

            Assert.Equal("0", output.ToString());
        }

        /// <summary>
        /// Whens the input is two output must be 2.
        /// </summary>
        [Fact]
        public void When_Input_Is_Two_Output_Must_Be_2()
        {
            int input = 2;

            string output = Changer.GetValue(input);

            Assert.Equal("2", output.ToString());
        }

        /// <summary>
        /// Whens the input is one output must be im even.
        /// </summary>
        [Fact]
        public void When_Input_Is_One_Output_Must_Be_ImEven()
        {
            int input = 1;

            string output = Changer.GetValue(input);

            Assert.Equal("ImEven", output.ToString());
        }
    }
}