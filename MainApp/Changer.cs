﻿namespace MainApp
{
    public class Changer
    {
        /// <summary>
        /// Gets the value.
        /// </summary>
        /// <param name="input">The input.</param>
        /// <returns></returns>
        public static string GetValue(int input)
        {
            if ((input % 2) != 0)
            {
                return "ImEven";
            }

            return input.ToString();
        }
    }
}