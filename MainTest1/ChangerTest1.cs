using MainApp;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace MainTest
{
    [TestClass]
    public class ChangerTest1
    {
        /// <summary>
        /// Whens the input is zero output must be 0.
        /// </summary>
        [TestMethod]
        public void When_Input_Is_Zero_Output_Must_Be_0()
        {
            int input = 0;

            string output = Changer.GetValue(input);

            Assert.AreEqual(0.ToString(), output.ToString());
        }

        /// <summary>
        /// Whens the input is two output must be 2.
        /// </summary>
        [TestMethod]
        public void When_Input_Is_Two_Output_Must_Be_2()
        {
            int input = 2;

            string output = Changer.GetValue(input);

            Assert.AreEqual(2.ToString(), output.ToString());
        }

        /// <summary>
        /// Whens the input is one output must be im even.
        /// </summary>
        [TestMethod]
        public void When_Input_Is_One_Output_Must_Be_ImEven()
        {
            int input = 1;

            string output = Changer.GetValue(input);

            Assert.AreEqual("ImEven", output.ToString());
        }
    }
}